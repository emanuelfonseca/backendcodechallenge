<?php

namespace Container7szPkVY;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder51898 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer732d6 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties256ad = [
        
    ];

    public function getConnection()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getConnection', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getMetadataFactory', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getExpressionBuilder', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'beginTransaction', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getCache', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getCache();
    }

    public function transactional($func)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'transactional', array('func' => $func), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'wrapInTransaction', array('func' => $func), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'commit', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->commit();
    }

    public function rollback()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'rollback', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getClassMetadata', array('className' => $className), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'createQuery', array('dql' => $dql), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'createNamedQuery', array('name' => $name), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'createQueryBuilder', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'flush', array('entity' => $entity), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'clear', array('entityName' => $entityName), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->clear($entityName);
    }

    public function close()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'close', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->close();
    }

    public function persist($entity)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'persist', array('entity' => $entity), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'remove', array('entity' => $entity), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'refresh', array('entity' => $entity), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'detach', array('entity' => $entity), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'merge', array('entity' => $entity), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getRepository', array('entityName' => $entityName), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'contains', array('entity' => $entity), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getEventManager', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getConfiguration', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'isOpen', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getUnitOfWork', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getProxyFactory', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'initializeObject', array('obj' => $obj), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'getFilters', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'isFiltersStateClean', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'hasFilters', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return $this->valueHolder51898->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer732d6 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder51898) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder51898 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder51898->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, '__get', ['name' => $name], $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        if (isset(self::$publicProperties256ad[$name])) {
            return $this->valueHolder51898->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder51898;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder51898;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder51898;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder51898;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, '__isset', array('name' => $name), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder51898;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder51898;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, '__unset', array('name' => $name), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder51898;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder51898;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, '__clone', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        $this->valueHolder51898 = clone $this->valueHolder51898;
    }

    public function __sleep()
    {
        $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, '__sleep', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;

        return array('valueHolder51898');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer732d6 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer732d6;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer732d6 && ($this->initializer732d6->__invoke($valueHolder51898, $this, 'initializeProxy', array(), $this->initializer732d6) || 1) && $this->valueHolder51898 = $valueHolder51898;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder51898;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder51898;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
