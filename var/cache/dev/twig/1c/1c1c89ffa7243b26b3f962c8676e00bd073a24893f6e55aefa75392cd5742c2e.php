<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* git_repo/index.html.twig */
class __TwigTemplate_c3f75db91a9a88a45f4b6e9f20fc219ed74b6ceb231d05dd61068451455e2398 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "git_repo/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "git_repo/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "git_repo/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    circunomics!
                        
                    
                
            
        
    
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 14
        echo "
    <div class=\"bg-dark text-secondary px-4 py-5 text-center\">
        <div class=\"py-5\">
            <p>
                <img src=\"https://static.wixstatic.com/media/45f9f0_4e8d1433ed374be0b2fb64fe74ea1aea~mv2.png/v1/fill/w_148,h_72,al_c,q_85,usm_0.66_1.00_0.01/Logo-Circunomics_white.webp\" alt=\"Logo-Circunomics_white.png\" style=\"width:74px;height:36px;object-fit:cover;object-position:50% 50%\"></p>

                <h1 class=\"display-5 fw-bold text-white\">
                    Circunomics Tech Challenge
                                                        
                                    
                </h1>
                <div class=\"col-md-9 mx-auto\">
                    <p class=\"fs-5 mb-4\">
                        Simple example with Symfony 5, Bootstrap 5, Docker, MailHog, Nginx, MySql and PHP MyAdmin by Emanuel Fonseca.                           
                    </p>
                        <table class=\"table table-dark table-hover table-responsive-lg fs-6\">
                            <thead>
                                <tr>
                                    <th scope=\"col\" class=\"col-md-2\">
                                        Id
                                                                            
                                    </th>
                                    <th scope=\"col\" class=\"col-md-2\">
                                        Date
                                                                                                                    
                                    </th>
                                    <th scope=\"col\" class=\"col-md-1\">
                                        Author
                                                                            
                                    </th>
                                    <th scope=\"col\" class=\"col-md-4\">
                                        Comment
                                                                            
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["dbCommits"]) || array_key_exists("dbCommits", $context) ? $context["dbCommits"] : (function () { throw new RuntimeError('Variable "dbCommits" does not exist.', 51, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["dbCommit"]) {
            // line 52
            echo "
";
            // line 53
            $context["lChar"] = twig_last($this->env, twig_get_attribute($this->env, $this->source, $context["dbCommit"], "commit_id", [], "any", false, false, false, 53));
            // line 54
            echo "
<tr>




                                        <td scope=\"row\" class=\"fs-6 text-start\" >
                                            ";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dbCommit"], "commit_id", [], "any", false, false, false, 61), "html", null, true);
            echo " ";
            if (preg_match("/^\\d+\$/", (isset($context["lChar"]) || array_key_exists("lChar", $context) ? $context["lChar"] : (function () { throw new RuntimeError('Variable "lChar" does not exist.', 61, $this->source); })()))) {
                echo "(last char is int) ";
            }
            // line 62
            echo "                                        </td>
                                        <td class=\"fs-6 text-start\">
                                            ";
            // line 64
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dbCommit"], "commited_at", [], "any", false, false, false, 64), "Y-m-d H:i:s"), "html", null, true);
            echo "
                                        </td>
                                        <td class=\"fs-6 text-start\">
                                            ";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dbCommit"], "author", [], "any", false, false, false, 67), "html", null, true);
            echo "
                                        </td>
                                        <td class=\"fs-6 text-start\">
                                            ";
            // line 70
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dbCommit"], "comment", [], "any", false, false, false, 70), "html", null, true);
            echo "
                                        </td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dbCommit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "git_repo/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 74,  175 => 70,  169 => 67,  163 => 64,  159 => 62,  153 => 61,  144 => 54,  142 => 53,  139 => 52,  135 => 51,  96 => 14,  86 => 13,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
    circunomics!
                        
                    
                
            
        
    
{% endblock %}

{% block body %}

    <div class=\"bg-dark text-secondary px-4 py-5 text-center\">
        <div class=\"py-5\">
            <p>
                <img src=\"https://static.wixstatic.com/media/45f9f0_4e8d1433ed374be0b2fb64fe74ea1aea~mv2.png/v1/fill/w_148,h_72,al_c,q_85,usm_0.66_1.00_0.01/Logo-Circunomics_white.webp\" alt=\"Logo-Circunomics_white.png\" style=\"width:74px;height:36px;object-fit:cover;object-position:50% 50%\"></p>

                <h1 class=\"display-5 fw-bold text-white\">
                    Circunomics Tech Challenge
                                                        
                                    
                </h1>
                <div class=\"col-md-9 mx-auto\">
                    <p class=\"fs-5 mb-4\">
                        Simple example with Symfony 5, Bootstrap 5, Docker, MailHog, Nginx, MySql and PHP MyAdmin by Emanuel Fonseca.                           
                    </p>
                        <table class=\"table table-dark table-hover table-responsive-lg fs-6\">
                            <thead>
                                <tr>
                                    <th scope=\"col\" class=\"col-md-2\">
                                        Id
                                                                            
                                    </th>
                                    <th scope=\"col\" class=\"col-md-2\">
                                        Date
                                                                                                                    
                                    </th>
                                    <th scope=\"col\" class=\"col-md-1\">
                                        Author
                                                                            
                                    </th>
                                    <th scope=\"col\" class=\"col-md-4\">
                                        Comment
                                                                            
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {% for dbCommit in dbCommits %}

{% set lChar = dbCommit.commit_id|last %}

<tr>




                                        <td scope=\"row\" class=\"fs-6 text-start\" >
                                            {{ dbCommit.commit_id }} {% if lChar matches '/^\\\\d+\$/' %}(last char is int) {% endif %}
                                        </td>
                                        <td class=\"fs-6 text-start\">
                                            {{ dbCommit.commited_at|date('Y-m-d H:i:s') }}
                                        </td>
                                        <td class=\"fs-6 text-start\">
                                            {{ dbCommit.author }}
                                        </td>
                                        <td class=\"fs-6 text-start\">
                                            {{ dbCommit.comment }}
                                        </td>
                                    </tr>
                                {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    {% endblock %}
    ", "git_repo/index.html.twig", "/var/www/templates/git_repo/index.html.twig");
    }
}
