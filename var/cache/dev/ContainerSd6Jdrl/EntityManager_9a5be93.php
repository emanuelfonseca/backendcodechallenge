<?php

namespace ContainerSd6Jdrl;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder76500 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerd103e = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties8edc1 = [
        
    ];

    public function getConnection()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getConnection', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getMetadataFactory', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getExpressionBuilder', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'beginTransaction', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getCache', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getCache();
    }

    public function transactional($func)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'transactional', array('func' => $func), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'wrapInTransaction', array('func' => $func), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'commit', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->commit();
    }

    public function rollback()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'rollback', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getClassMetadata', array('className' => $className), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'createQuery', array('dql' => $dql), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'createNamedQuery', array('name' => $name), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'createQueryBuilder', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'flush', array('entity' => $entity), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'clear', array('entityName' => $entityName), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->clear($entityName);
    }

    public function close()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'close', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->close();
    }

    public function persist($entity)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'persist', array('entity' => $entity), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'remove', array('entity' => $entity), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'refresh', array('entity' => $entity), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'detach', array('entity' => $entity), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'merge', array('entity' => $entity), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getRepository', array('entityName' => $entityName), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'contains', array('entity' => $entity), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getEventManager', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getConfiguration', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'isOpen', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getUnitOfWork', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getProxyFactory', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'initializeObject', array('obj' => $obj), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'getFilters', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'isFiltersStateClean', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'hasFilters', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return $this->valueHolder76500->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerd103e = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder76500) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder76500 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder76500->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, '__get', ['name' => $name], $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        if (isset(self::$publicProperties8edc1[$name])) {
            return $this->valueHolder76500->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder76500;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder76500;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder76500;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder76500;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, '__isset', array('name' => $name), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder76500;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder76500;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, '__unset', array('name' => $name), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder76500;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder76500;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, '__clone', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        $this->valueHolder76500 = clone $this->valueHolder76500;
    }

    public function __sleep()
    {
        $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, '__sleep', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;

        return array('valueHolder76500');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerd103e = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerd103e;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerd103e && ($this->initializerd103e->__invoke($valueHolder76500, $this, 'initializeProxy', array(), $this->initializerd103e) || 1) && $this->valueHolder76500 = $valueHolder76500;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder76500;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder76500;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
