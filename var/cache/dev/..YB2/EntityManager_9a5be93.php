<?php

namespace ContainerMws49Sk;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHoldere0442 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer83dc9 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties91c5a = [
        
    ];

    public function getConnection()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getConnection', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getMetadataFactory', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getExpressionBuilder', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'beginTransaction', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getCache', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getCache();
    }

    public function transactional($func)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'transactional', array('func' => $func), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'wrapInTransaction', array('func' => $func), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'commit', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->commit();
    }

    public function rollback()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'rollback', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getClassMetadata', array('className' => $className), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'createQuery', array('dql' => $dql), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'createNamedQuery', array('name' => $name), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'createQueryBuilder', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'flush', array('entity' => $entity), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'clear', array('entityName' => $entityName), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->clear($entityName);
    }

    public function close()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'close', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->close();
    }

    public function persist($entity)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'persist', array('entity' => $entity), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'remove', array('entity' => $entity), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'refresh', array('entity' => $entity), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'detach', array('entity' => $entity), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'merge', array('entity' => $entity), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getRepository', array('entityName' => $entityName), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'contains', array('entity' => $entity), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getEventManager', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getConfiguration', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'isOpen', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getUnitOfWork', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getProxyFactory', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'initializeObject', array('obj' => $obj), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'getFilters', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'isFiltersStateClean', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'hasFilters', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return $this->valueHoldere0442->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer83dc9 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHoldere0442) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHoldere0442 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHoldere0442->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, '__get', ['name' => $name], $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        if (isset(self::$publicProperties91c5a[$name])) {
            return $this->valueHoldere0442->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere0442;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldere0442;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere0442;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldere0442;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, '__isset', array('name' => $name), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere0442;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldere0442;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, '__unset', array('name' => $name), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere0442;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldere0442;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, '__clone', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        $this->valueHoldere0442 = clone $this->valueHoldere0442;
    }

    public function __sleep()
    {
        $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, '__sleep', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;

        return array('valueHoldere0442');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer83dc9 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer83dc9;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer83dc9 && ($this->initializer83dc9->__invoke($valueHoldere0442, $this, 'initializeProxy', array(), $this->initializer83dc9) || 1) && $this->valueHoldere0442 = $valueHoldere0442;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldere0442;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHoldere0442;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
