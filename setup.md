Battery:
PHP 7.4
MySql
Nginx
Symfony 5 + Doctrine
Booststrap 5
Mail Hog
PHP MyAdmin
docker
Docker-Compose

Details:

The logic was to import the last 30 commits through the api github publishes, and when loading the insert strategy that does not exist in the database, and if so, just update the record that already exists. Thus avoiding duplication. Simple and fast way, as it was not requested to create a CRUD. I used annotations for routes and entities, repository default, with ASC author ordering. Regarding the last character check, I've chosen (for design reasons) to put it in text if it's a number. For database i used doctrine migrations. Time spent total : 3 hours. 

To run the docker, just the command

docker-composer up build -d