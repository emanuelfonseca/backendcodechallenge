FROM php:7.4-fpm

RUN apt-get update && apt-get install -y \
    gnupg \
    g++ \
    procps \
    openssl \
    git \
    unzip \
    zlib1g-dev \
    libzip-dev \
    libfreetype6-dev \
    libpng-dev \
    libjpeg-dev \
    libicu-dev  \
    libonig-dev \
    libxslt1-dev \
    acl \
    && echo 'alias sf="php bin/console"' >> ~/.bashrc

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-source extract \
    && docker-php-ext-configure mysqli --with-mysqli=mysqlnd \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-configure gd \
    && docker-php-ext-install exif gd mbstring intl xsl zip mysqli pdo_mysql soap bcmath pcntl \
    && docker-php-ext-enable opcache 

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN usermod -u 1000 www-data

RUN chown -R www-data:www-data /var/www

USER www-data

WORKDIR /usr/src/app

CMD php-fpm

EXPOSE 9000
