<?php

namespace App\Controller;

use App\Entity\GitRepo;
use App\Repository\GitRepoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GitRepoController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(GitRepoRepository $gitRepository): Response
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://api.github.com/repos/nodejs/node/commits');

        $lastCommits = $response->toArray();

        $entityManager = $this->getDoctrine()->getManager();

        $git = new GitRepo();

        array_walk($lastCommits, function ($value, $key) use ($git, $entityManager) {

            $git->setAuthor($value['commit']['author']['name']);
            $git->setComment($value['commit']['message']);
            $git->setCommit_Id($value['sha']);
            $git->setCommited_At(new \DateTime('@' . strtotime($value['commit']['committer']['date'])));

            $entityManager->merge($git);

        });

        $entityManager->flush();

        $dbCommits = $gitRepository->findBy([], ['author' => 'DESC'], 10);

        return $this->render('git_repo/index.html.twig', [
            'dbCommits' => $dbCommits,
        ]);

        
    }

}
