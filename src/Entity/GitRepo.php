<?php

namespace App\Entity;

use App\Repository\GitRepoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GitRepoRepository::class)
 */
class GitRepo
{

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $commited_at;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=48, nullable=true)
     */
    private $commit_id;

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCommited_At(): ?\DateTimeInterface
    {
        return $this->commited_at;
    }

    public function setCommited_At(?\DateTimeInterface $commited_at): self
    {
        $this->commited_at = $commited_at;

        return $this;
    }

    public function getCommit_Id(): ?string
    {
        return $this->commit_id;
    }

    public function setCommit_Id(?string $commit_id): self
    {
        $this->commit_id = $commit_id;

        return $this;
    }

}
